class User {
    constructor(userName, password, firstName, surname, country, birthday, gender, hobbies) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.surname = surname;
        this.country = country;
        this.birthday = birthday;
        this.gender = gender;
        this.hobbies = hobbies;
    }
}

export default User;
